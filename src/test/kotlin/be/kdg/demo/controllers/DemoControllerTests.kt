package be.kdg.demo.controllers

import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.*
import org.springframework.boot.test.autoconfigure.web.servlet.*
import org.springframework.boot.test.context.*
import org.springframework.test.web.servlet.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@SpringBootTest
@AutoConfigureMockMvc
class DemoControllerTests {
    @Autowired
    private lateinit var mvc: MockMvc

    @Test
    fun `GET request should return hello world`() {
        mvc.perform(
                get("/"))
                .andExpect(status().isOk)
                .andExpect(header().string("Content-Type", "text/plain; charset=ascii"))
                .andExpect(content().string("Hello, world!"))
    }
}
